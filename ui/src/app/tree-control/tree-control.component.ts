import { FlatTreeControl } from '@angular/cdk/tree';
import { Component } from '@angular/core';
import { MatTreeModule } from '@angular/material/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { AppService } from './../app.service';

/**
* Food data with nested structure.
* Each node has a name and an optiona list of children.
*/
interface DataNode {
  name: string;
  nodes?: DataNode[];
  id: number;
}

/** Flat node with expandable and level information */
interface TreeNode {
  expandable: boolean;
  name: string;
  level: number;
  id: number;
}

/**
* @title Tree with flat nodes
*/
@Component({
  selector: 'tree-control',
  templateUrl: 'tree-control.component.html',
  styleUrls: ['tree-control.component.css'],
})

export class TreeControlComponent {
  private transformer = (node: DataNode, level: number) => {
    return {
      expandable: !!node.nodes && node.nodes.length > 0,
      name: node.name,
      level: level,
      id: node.id
    };
  }

  treeControl = new FlatTreeControl<TreeNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.nodes);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(private appService: AppService) {
    this.dataSource.data = [];
    this.appService.getTreeData().subscribe((data: any) => {
      this.dataSource.data = data;
    });
  }

  hasChild = (_: number, node: TreeNode) => node.expandable;
}
