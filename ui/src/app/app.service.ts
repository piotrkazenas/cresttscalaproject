import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/index';

/**
 * Class representing application service.
 *
 * @class AppService.
 */
@Injectable()
export class AppService {
  private serviceUrl = '/api/summary';
  private dataPostTestUrl = '/api/postTest';
  private loadDataUrl = '/api/loadData';

  constructor(private http: HttpClient) {
  }
  public getTreeData() {
    return this.http.get(this.loadDataUrl).pipe(
      map(response => response)
    );
  }
}
