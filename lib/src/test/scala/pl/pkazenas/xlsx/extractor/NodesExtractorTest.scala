package pl.pkazenas.xlsx.extractor

import org.scalatest.FunSuite
import pl.pkazenas.xlsx.extractor.NodesExtractor.Node
import pl.pkazenas.xlsx.parser.TestCommons

class NodesExtractorTest extends FunSuite with TestCommons {
  val expectedData =
    List(
      Node(id = 1, name = "A", List(
        Node(id = 2, name = "AA", List(
          Node(id = 3, name = "AA1"),
          Node(id = 4, name = "AA2")
        )),
        Node(id = 5, name = "AB")
      )),
      Node(id = 6, name = "B"),
      Node(id = 7, name = "C", List(
        Node(id = 8, name = "CA", List(
          Node(id = 9, name = "CA1"),
          Node(id = 10, name = "CA2")
        ))
      )),
      Node(id = 11, name = "D", List(
        Node(id = 12, name = "DA")
      ))
    )

  test("Parse file test1.xlsx") {
    val extractor = NodesExtractor()
    assertResult(expectedData)(extractor.extractNodes(xlsxPath))
  }

  test("Parse non-existing file") {
    val extractor = NodesExtractor()
    assertResult(List())(extractor.extractNodes("invalidPath"))
  }
}
