package pl.pkazenas.xlsx.parser

import org.scalatest.FunSuite
import scala.io.Source
import cats.implicits._

class XlsxUtilsTest extends FunSuite with TestCommons {
  test("Load workbook - positive") {
    val workbook = XlsxWorkbook(xlsxPath)
    assert(workbook.isSuccess)
    workbook.foreach(_.close())
  }

  test("Load workbook - negative") {
    val workbook = XlsxWorkbook("/invalidFile.xlsx")
    assert(workbook.isFailure)
  }

  def accessWorkbook(f: XlsxWorkbook => Unit) = {
    XlsxWorkbook(xlsxPath)
      .foreach(workbook => {
        f(workbook)
        workbook.close()
      })
  }

  test("Access workbooks sheets") {
    accessWorkbook { workbook =>
      // try to access existing sheet
      val sheet1 = workbook.getSheetAt(0)
      assert(sheet1.isDefined)

      // try to access nonexisting sheet
      val sheet2 = workbook.getSheetAt(1)
      assert(!sheet2.isDefined)
    }
  }

  test("Read rows data") {
    accessWorkbook { workbook =>
      val sheet = workbook.getSheetAt(0)
      assert(sheet.isDefined)

      sheet.foreach { sheet =>
        val row0 = sheet.getRow(0)
        assert(row0.isDefined)
        row0.foreach { row =>
          assertResult("Poziom 1".some)(row.stringValueAt(0))
          assertResult("ID".some)(row.stringValueAt(3))
          assertResult(none)(row.stringValueAt(4))
        }

        val row1 = sheet.getRow(1)
        assert(row1.isDefined)
        row1.foreach { row =>
          assertResult("A".some)(row.stringValueAt(0))
          assertResult(none)(row.stringValueAt(1))
          assertResult(1.some)(row.intValueAt(3))
        }

        val row13 = sheet.getRow(13)
        assert(row13.isDefined)
        row13.foreach { row =>
          assertResult(none)(row.stringValueAt(0))
          assertResult(none)(row.stringValueAt(1))
          assertResult(none)(row.stringValueAt(2))
          assertResult(none)(row.stringValueAt(3))
        }
      }
    }
  }
}
