package pl.pkazenas.xlsx.parser

import org.apache.poi.ss.usermodel.Row

import scala.util.Try

trait XlsxRow {
  def stringValueAt(index: Int): Option[String]
  def numberValueAt(index: Int): Option[Double]
  def intValueAt(index: Int): Option[Int]
}

class XlsxRowImpl(row: Row) extends XlsxRow {
  private def cellAt(index: Int) = row.getCell(index, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL)

  override def stringValueAt(index: Int): Option[String] = {
    Try(Option(cellAt(index).getStringCellValue))
      .toOption
      .flatten
  }

  override def numberValueAt(index: Int): Option[Double] = {
    Try(cellAt(index).getNumericCellValue)
      .toOption
  }

  override def intValueAt(index: Int): Option[Int] =
    numberValueAt(index).map(_.toInt)
}

object XlsxRow {
  def apply(row: Row): XlsxRow = new XlsxRowImpl(row)
}
