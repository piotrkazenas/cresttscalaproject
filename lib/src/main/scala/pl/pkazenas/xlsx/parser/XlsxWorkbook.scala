package pl.pkazenas.xlsx.parser
import org.apache.poi.ss.usermodel._
import java.io.File

import collection.JavaConverters._
import scala.util.Try

trait XlsxWorkbook {
  def getSheetAt(index: Int): Option[XlsxSheet]
  def close(): Unit
}

class XlsxWorkbookImpl(workbook: Workbook) extends XlsxWorkbook{
  override def getSheetAt(index: Int): Option[XlsxSheet] = {
    Try(workbook.getSheetAt(index))
      .map(XlsxSheet(_))
      .toOption
  }

  override def close(): Unit = workbook.close()
}

object XlsxWorkbook {
  def apply(path: String): Try[XlsxWorkbook] = Try {
    val file = new File(path)
    val workbook = WorkbookFactory.create(file)
    new XlsxWorkbookImpl(workbook)
  }
}
