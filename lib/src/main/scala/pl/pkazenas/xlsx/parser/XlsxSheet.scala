package pl.pkazenas.xlsx.parser

import org.apache.poi.ss.usermodel.Sheet

import scala.util.Try

trait XlsxSheet {
  def getRow(index: Int): Option[XlsxRow]
}


class XlsxSheetImpl(sheet: Sheet) extends XlsxSheet {
  override def getRow(index: Int): Option[XlsxRow] = {
    Try(sheet.getRow(index))
      .map(XlsxRow(_))
      .toOption
  }
}

object XlsxSheet {
  def apply(sheet: Sheet): XlsxSheet = new XlsxSheetImpl(sheet)
}