package pl.pkazenas.xlsx

import pl.pkazenas.xlsx.extractor.NodesExtractor
import io.circe.generic.auto._
import io.circe.syntax._
import pl.pkazenas.xlsx.json.NodeListJsonConverter

object Main extends App {
  val xlsxPath = getClass.getResource("/test1.xlsx").getPath
  val extractor = NodesExtractor()
  val parsedXlsx = extractor.extractNodes(xlsxPath)

  println(NodeListJsonConverter.convert(parsedXlsx))
}
