package pl.pkazenas.xlsx.extractor

import NodesExtractor._
import pl.pkazenas.xlsx.parser.{XlsxSheet, XlsxWorkbook}

class NodesExtractor(maxLevel: Int, startRowIndex: Int, startCellIndex: Int, sheetIndex: Int) {
  private def extractAllNodes(sheet: XlsxSheet, rowIndex: Int, cellIndex: Int, nodes: List[Node], depth: Int): (List[Node], Int) = {
    // read basic node data (without subnodes) to determine if node at given position exists
    val node =
      for {
        row <- sheet.getRow(rowIndex)
        nodeName <- row.stringValueAt(cellIndex)
        id <- row.intValueAt(maxLevel + 1)
      } yield Node(id = id, name = nodeName)

    node.fold {
      (nodes, depth)
    }(node => {
      // if this is last level add this node to subnodes list and traverse to next row - same cell
      if (cellIndex == maxLevel) extractAllNodes(sheet, rowIndex + 1, cellIndex, node :: nodes, depth + 1)
      else {
        val subnodes = extractAllNodes(sheet, rowIndex + 1, cellIndex + 1, List(), 1)
        val nodeWithSubnodes = node.copy(nodes = subnodes._1.reverse)
        extractAllNodes(sheet, rowIndex + subnodes._2, cellIndex, nodeWithSubnodes :: nodes, depth + subnodes._2)
      }
    })
  }

  def extractNodes(xlsxFilePath: String): List[Node] = {
    def extractFromSheet(sheet: XlsxSheet): List[Node] = {
      val (nodes, depth) = extractAllNodes(sheet, startRowIndex, 0, List(), 0)
      nodes.reverse
    }

    XlsxWorkbook(xlsxFilePath)
      .toOption
      .map(workbook => {
        val result =
          workbook.getSheetAt(0).fold[List[Node]] {
            List()
          }(sheet => extractFromSheet(sheet))

        // close workbook
        workbook.close()

        result
      })
      .getOrElse(List())
  }
}

object NodesExtractor {

  case class Node(id: Int, name: String, nodes: List[Node] = List())

  def apply(maxLevel: Int = 2, startRowIndex: Int = 1, startCellIndex: Int = 0, sheetIndex: Int = 0): NodesExtractor =
    new NodesExtractor(maxLevel, startRowIndex, startCellIndex, sheetIndex)
}
