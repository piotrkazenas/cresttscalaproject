package pl.pkazenas.xlsx.json

import pl.pkazenas.xlsx.extractor.NodesExtractor.Node
import io.circe.generic.auto._, io.circe.syntax._

object NodeListJsonConverter {
  def convert(nodes: List[Node]): String = {
    nodes.asJson.spaces2
  }
}
