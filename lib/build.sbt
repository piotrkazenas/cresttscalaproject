name := "cresttscalaproject"

version := "0.1"

scalaVersion := "2.12.8"

val circeVersion = "0.10.0"
val circeDependencies =  Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

val testDependencies = Seq(
  "org.scalactic" %% "scalactic" % "3.0.5" % "test",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)

val miscDependencies = Seq(
  "org.typelevel" %% "cats-core" % "1.6.0",
  "org.apache.poi" % "poi-ooxml" % "4.0.1"

)

libraryDependencies ++= circeDependencies ++ testDependencies ++ miscDependencies

mainClass in (Compile, run) := Some("pl.pkazenas.xlsx.Main")
