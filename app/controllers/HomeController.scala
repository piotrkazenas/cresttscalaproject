package controllers

import javax.inject._

import pl.pkazenas.xlsx.Main.getClass
import play.api.libs.json.Json
import play.api.mvc._
import pl.pkazenas.xlsx._
import pl.pkazenas.xlsx.extractor.NodesExtractor
import pl.pkazenas.xlsx.json.NodeListJsonConverter

@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  val xlsxPath = getClass.getResource("/test1.xlsx").getPath
  val extractor = NodesExtractor()

  def loadData = Action {
    val parsedXlsx = extractor.extractNodes(xlsxPath)
    Ok(Json.parse(NodeListJsonConverter.convert(parsedXlsx)))
  }

}
