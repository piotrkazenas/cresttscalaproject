name := """crestt-scala-project"""

version := "1.0-SNAPSHOT"


resolvers += Resolver.sonatypeRepo("snapshots")

scalaVersion := "2.12.2"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
libraryDependencies += "com.h2database" % "h2" % "1.4.196"

lazy val root =
  (project in file("."))
    .enablePlugins(PlayScala)
    .aggregate(lib)
    .dependsOn(lib)
    .settings(
      watchSources ++= (baseDirectory.value / "public/ui" ** "*").get
    )

lazy val lib = project